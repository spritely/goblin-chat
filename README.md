Goblin Chat
-----------

A basic chat application using [Goblins](https://gitlab.com/spritely/goblins) 

This is experimental software.  You gotta keep upgrading with the git
repo to keep on top of things because the protocol is in flux!


Usage
-----

First you need to setup tor and a few things for Goblins, note you
need to use the latest goblins git version, to do this run the
following command:

```
$ raco pkg install --type git-url "https://gitlab.com/spritely/goblins.git?path=goblins"
```

Once goblins is installed, make sure you have tor and follow the setup
instructions [here](https://docs.racket-lang.org/goblins/captp.html)
and follow the steps in section 5.3.1 "Launch a Tor Deamon for
Goblins". Once you've done that follow the steps below.

You need to install three additional racket package, aside from the
"goblins" package. To do that use:
```
$ raco pkg install aurie
$ raco pkg install --type git-url "https://gitlab.com/spritely/brux.git?path=brux"
$ raco pkg install --type git-url "https://gitlab.com/spritely/hashvatar.git"
```

To run the server you have someone first do:

```
$ racket onion-gui-server.rkt <channel-name> <username>

```

This will open the GUI and provide you with a URL which you should
share with other users who would like to connect.


To use the goblin chat client, you do:
```
$ racket onion-gui-client.rkt <url> <username>
```

All done!

Troubleshooting
---------------

There are several state files made by goblins, these sometimes have
problems when upgrades have been made. These files are always made in
the current working directory which you launch goblin chat from. Try
deleting these if you run into problems:

- "personas.state" & "personas.state.roots"
- "chatroom.state" & "chatroom.state.roots"
- "onion.state" & "onion.state.roots"

License
-------

Apache v2. See the LICENCE.txt file for more information.
